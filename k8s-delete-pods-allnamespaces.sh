#!/bin/bash

# Warn user about the consequences of running the script
echo -e "\e[5;1;41m WARNING                                                                                                           \e[0m"
echo -e "\e[1;41m This will delete ALL PODS in ALL NAMESPACES that are not in state: Running, Pending, ContainerCreating, Completed \e[0m"
echo -e "\e[5;1;41m WARNING                                                                                                           \e[0m"

# Ask for confirmation and run script
while true; do 
    echo -e "\e[0;33mAre you sure you want to proceed? (yes/no)\e[0m"
    read response
    case "$response" in
        yes)
            echo "Deleting all pods in ALL NAMESPACES"
            namespaces=$(kubectl get ns --no-headers | awk '{print $1}')
            for ns in $namespaces; do
                # Henter alle pods som ikke er i ønskede tilstander
                pods_to_delete=$(kubectl get pods -n "$ns" --no-headers | awk '/Running|Pending|ContainerCreating|Completed/ {next} {print $1}')
                if [ -n "$pods_to_delete" ]; then
                    # Sletter kun pods hvis det faktisk er noen å slette
                    echo "Deleting non-active pods in namespace $ns"
                    kubectl delete pods -n "$ns" $pods_to_delete
                else
                    echo "No pods to delete in namespace $ns"
                fi
            done
            break
            ;;
        no)
            echo "I guess you regretted it?"
            exit 0
            ;;
        *)
            echo "Invalid response. Please try again."
            ;;
    esac
done
